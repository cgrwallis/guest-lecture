from dog_cat_classifier import get_prediction_model
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers

import matplotlib.pyplot as plt



def main():

    test_data_dir = 'data/test'
    img_width=150
    img_height=150
    nb_test_samples = 160
    batch_size = 1

    model = get_prediction_model('weights/stage_3.h5', img_width, img_height)

    test_datagen = ImageDataGenerator(rescale=1. / 255)

    test_generator = test_datagen.flow_from_directory(
        test_data_dir,
        target_size=(img_height, img_width),
        batch_size=batch_size,
        class_mode='binary')

    # show generally the scores

    score = model.evaluate_generator(test_generator, nb_test_samples/batch_size)
    
    print("Test loss     : {}".format(score[0]))
    print("Test accuracy : {}".format(score[1]))


    good_count = 0
    good_list = []
    bad_count = 0
    bad_list = []
    n_show = 5

    for i_batch in range(nb_test_samples):
        X, y = test_generator.__getitem__(i_batch)
        y_pred = model.predict(X)

        if abs(y_pred[0][0]-y[0]) < 0.5:
            if good_count < n_show:
                good_list.append((X[0], y_pred[0], y[0]))
                good_count += 1
        else:
            if bad_count > n_show:
                break
            else:
                bad_list.append((X[0], y_pred[0], y[0]))
                bad_count += 1

    # show some good ones
    for X, y_pred, y in good_list:
        plt.figure()
        plt.imshow(X)
        plt.title("good one {y_pred} - {y} ".format(y_pred=y_pred, y=y))
    plt.show()
    
    # show and bad ones

    for X, y_pred, y in bad_list:
        plt.figure()
        plt.imshow(X)
        plt.title("bad one {y_pred} - {y} ".format(y_pred=y_pred, y=y))
    plt.show()


    good_example = good_list[0][0]
    bad_example = bad_list[0][0]

if __name__ in "__main__":
    main()