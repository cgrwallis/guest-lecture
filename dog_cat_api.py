from flask import Flask, request, jsonify
from io import BytesIO
from PIL import Image
import logging
import numpy as np
import tensorflow as tf

from dog_cat_classifier import get_prediction_model

IMG_WIDTH = 150
IMG_HEIGHT = 150
MODEL = None


ALLOWED_EXTENSIONS = set(['jpg', 'jpeg'])

app = Flask(__name__)

def load_model():
    global MODEL
    MODEL = get_prediction_model('weights/stage_3.h5', IMG_WIDTH, IMG_HEIGHT)
    global GRAPH
    GRAPH = tf.get_default_graph()


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def get_file_input_and_check_is_good(request_files):
    # factor the file checking out

    if 'file' not in request_files:
        abort(400)

    fileobj = request_files['file']
    # if user does not select file, browser also
    # submit an empty part without filename
    if fileobj.filename == '':
        abort(400)

    if fileobj and allowed_file(fileobj.filename):
        return fileobj
    else:
        abort(400)

@app.errorhandler(400)
def incorrect_inputs(error):
    return make_response(jsonify({'error': 'Incorrect inputs'}), 400)

def make_prediction(fileobj):
    img = Image.open(fileobj)
    img = img.resize((IMG_WIDTH, IMG_HEIGHT))

    pix = np.array(img.getdata()).reshape(1, img.size[0], img.size[1], 3)
    pix = pix/255

    global GRAPH
    with GRAPH.as_default():
        prediction = MODEL.predict(pix)

    if prediction[0][0] > 0.5:
        return {"animal": "dog"}
    else:
        return {"animal": "cat"}    

@app.route('/dog_cat/api/v1.0/classify', methods=['GET'])
def is_it_a_dog_or_a_cat():
    fileobj = get_file_input_and_check_is_good(request.files)

    return jsonify(make_prediction(fileobj))

if __name__ == '__main__':
    load_model()
    app.run(debug=True)