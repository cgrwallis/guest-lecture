'''This script goes along the blog post
"Building powerful image classification models using very little data"
from blog.keras.io.
It uses data that can be downloaded at:
https://www.kaggle.com/c/dogs-vs-cats/data
'''

from keras import applications
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense

def transferlearning_model(input_shape):
    # build the VGG16 network
    VGGmodel = applications.VGG16(weights='imagenet', include_top=False, input_shape = input_shape)
    print('Model loaded.')

    model = Sequential()
    for layer in VGGmodel.layers: 
        model.add(layer)

    model.add(Flatten())
    model.add(Dense(256, activation='relu', name="final_feature_collection"))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid', name="final_decision"))


    return model

def get_prediction_model(weight_path, img_width, img_height):

    model = transferlearning_model((img_width, img_height, 3))

    model.load_weights(weight_path, by_name=True)

    model.compile(loss='binary_crossentropy',
                  optimizer=optimizers.SGD(lr=0.01, momentum=0.9),
                  metrics=['accuracy'])
    return model

def train_model(input_weights=None, output_weights=None, lr=1e-2, 
                epochs=5, train_row=set()):
    
    train_data_dir='data/train'
    validation_data_dir='data/val'
    img_width=150
    img_height=150
    nb_train_samples = 6000
    nb_validation_samples = 1000
    batch_size = 16

    model = transferlearning_model((img_width, img_height, 3))

    # # add the model on top of the convolutional base
    # model.add(top_model)

    # set the only the final layers to be trained
    # to non-trainable (weights will not be updated)
 
    train_by_name = []
    block_base = ['_conv1', '_conv2', '_conv3', '_pool']
    for row in train_row:
        block_list = ['block'+str(row)+ending for ending in block_base]
        train_by_name += block_list

    train_by_name += ['final_feature_collection', 'final_decision', 'flatten_1', 'dropout_1']

    for i_layer, layer in enumerate(model.layers):
        if layer.name in train_by_name:
            layer.trainable = True
        else:
            layer.trainable = False
        print(layer.name, layer.trainable)
    # compile the model with a SGD/momentum optimizer
    # and a very slow learning rate.
    if input_weights is not None:
        model.load_weights(input_weights, by_name=True)

    print(model.summary)

    model.compile(loss='binary_crossentropy',
                  optimizer=optimizers.SGD(lr=lr, momentum=0.9),
                  metrics=['accuracy'])

    # prepare data augmentation configuration
    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0.1,
        zoom_range=0.1,
        horizontal_flip=True)

    test_datagen = ImageDataGenerator(rescale=1. / 255)

    train_generator = train_datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_height, img_width),
        batch_size=batch_size,
        class_mode='binary')

    validation_generator = test_datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_height, img_width),
        batch_size=batch_size,
        class_mode='binary')

    # fine-tune the model
    model.fit_generator(
        train_generator,
        steps_per_epoch=nb_train_samples // batch_size,
        epochs=epochs,
        validation_data=validation_generator,
        validation_steps=nb_validation_samples // batch_size)

    if output_weights is not None:
        model.save(output_weights)

def main():

    train_model(input_weights=None, output_weights='weights/stage_1.h5', lr=1e-3, 
                epochs=5, train_row=[])

    train_model(input_weights='weights/stage_1.h5', output_weights='weights/stage_2.h5', lr=1e-3,
                epochs=5, train_set=[5])

    train_model(input_weights='weights/stage_2.h5', output_weights='weights/stage_3.h5', lr=1e-4,
                epochs=5, train_set=[4,5])


if __name__ == "__main__":
    main()
