This creates train, validataion and test sets.

```cp dogs-vs-cats/train/cat.1???.jpg data/train/cats/
cp dogs-vs-cats/train/cat.2???.jpg data/train/cats/
cp dogs-vs-cats/train/cat.3???.jpg data/train/cats/

cp dogs-vs-cats/train/cat.4???.jpg data/test/cats/

cp dogs-vs-cats/train/cat.5???.jpg data/val/cats/

cp dogs-vs-cats/train/dog.1???.jpg data/train/dogs/
cp dogs-vs-cats/train/dog.2???.jpg data/train/dogs/
cp dogs-vs-cats/train/dog.3???.jpg data/train/dogs/

cp dogs-vs-cats/train/dog.4???.jpg data/test/dogs/

cp dogs-vs-cats/train/dog.5???.jpg data/val/dogs/
```

cwallis@tensorflow-1-vm:~/guest-lecture$ python dog_cat_classifier.py 
Using TensorFlow backend.
WARNING:tensorflow:From /usr/local/lib/python2.7/dist-packages/tensorflow/python/framework/op_def_library.py:263: colocate_with (from tensorflow.python.framework.ops) is deprecated and will be removed in a future version.
Instructions for updating:
Colocations handled automatically by placer.
2019-03-06 15:54:32.973509: I tensorflow/core/platform/cpu_feature_guard.cc:141] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
2019-03-06 15:54:34.989580: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:998] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2019-03-06 15:54:34.990138: I tensorflow/compiler/xla/service/service.cc:150] XLA service 0x560cc27b8d20 executing computations on platform CUDA. Devices:
2019-03-06 15:54:34.990192: I tensorflow/compiler/xla/service/service.cc:158]   StreamExecutor device (0): Tesla K80, Compute Capability 3.7
2019-03-06 15:54:34.993043: I tensorflow/core/platform/profile_utils/cpu_utils.cc:94] CPU Frequency: 2200000000 Hz
2019-03-06 15:54:34.993630: I tensorflow/compiler/xla/service/service.cc:150] XLA service 0x560cc2821e90 executing computations on platform Host. Devices:
2019-03-06 15:54:34.993682: I tensorflow/compiler/xla/service/service.cc:158]   StreamExecutor device (0): <undefined>, <undefined>
2019-03-06 15:54:34.994077: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1433] Found device 0 with properties: 
name: Tesla K80 major: 3 minor: 7 memoryClockRate(GHz): 0.8235
pciBusID: 0000:00:04.0
totalMemory: 11.17GiB freeMemory: 11.10GiB
2019-03-06 15:54:34.994116: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1512] Adding visible gpu devices: 0
2019-03-06 15:54:34.994929: I tensorflow/core/common_runtime/gpu/gpu_device.cc:984] Device interconnect StreamExecutor with strength 1 edge matrix:
2019-03-06 15:54:34.994963: I tensorflow/core/common_runtime/gpu/gpu_device.cc:990]      0 
2019-03-06 15:54:34.994972: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1003] 0:   N 
2019-03-06 15:54:34.995246: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1115] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 10802 MB memory) -> physical GPU (device: 0, name: Tesla K80, pci bus id: 0000:00:04.0, compute capability: 3.7)
Model loaded.
WARNING:tensorflow:From /usr/local/lib/python2.7/dist-packages/keras/backend/tensorflow_backend.py:3445: calling dropout (from tensorflow.python.ops.nn_ops) with keep_prob is deprecated and will be removed in a future version.
Instructions for updating:
Please use `rate` instead of `keep_prob`. Rate should be set to `rate = 1 - keep_prob`.
('block1_conv1', False)
('block1_conv2', False)
('block1_pool', False)
('block2_conv1', False)
('block2_conv2', False)
('block2_pool', False)
('block3_conv1', False)
('block3_conv2', False)
('block3_conv3', False)
('block3_pool', False)
('block4_conv1', False)
('block4_conv2', False)
('block4_conv3', False)
('block4_pool', False)
('block5_conv1', False)
('block5_conv2', False)
('block5_conv3', False)
('block5_pool', False)
('flatten_1', True)
('final_feature_collection', True)
('dropout_1', True)
('final_decision', True)
<bound method Sequential.summary of <keras.engine.sequential.Sequential object at 0x7f2138cd9350>>
Found 6000 images belonging to 2 classes.
Found 2000 images belonging to 2 classes.
WARNING:tensorflow:From /usr/local/lib/python2.7/dist-packages/tensorflow/python/ops/math_ops.py:3066: to_int32 (from tensorflow.python.ops.math_ops) is deprecated and will be removed in a future version.
Instructions for updating:
Use tf.cast instead.
Epoch 1/5
2019-03-06 15:54:37.430879: I tensorflow/stream_executor/dso_loader.cc:152] successfully opened CUDA library libcublas.so.10.0 locally
375/375 [==============================] - 54s 145ms/step - loss: 0.4634 - acc: 0.7730 - val_loss: 0.3442 - val_acc: 0.8357
Epoch 2/5
375/375 [==============================] - 52s 138ms/step - loss: 0.3575 - acc: 0.8422 - val_loss: 0.4196 - val_acc: 0.7944
Epoch 3/5
375/375 [==============================] - 52s 138ms/step - loss: 0.3336 - acc: 0.8525 - val_loss: 0.2676 - val_acc: 0.8901
Epoch 4/5
375/375 [==============================] - 51s 136ms/step - loss: 0.3158 - acc: 0.8603 - val_loss: 0.3231 - val_acc: 0.8488
Epoch 5/5
375/375 [==============================] - 52s 139ms/step - loss: 0.2960 - acc: 0.8715 - val_loss: 0.2565 - val_acc: 0.9012
Model loaded.
('block1_conv1', False)
('block1_conv2', False)
('block1_pool', False)
('block2_conv1', False)
('block2_conv2', False)
('block2_pool', False)
('block3_conv1', False)
('block3_conv2', False)
('block3_conv3', False)
('block3_pool', False)
('block4_conv1', False)
('block4_conv2', False)
('block4_conv3', False)
('block4_pool', False)
('block5_conv1', True)
('block5_conv2', True)
('block5_conv3', True)
('block5_pool', True)
('flatten_2', False)
('final_feature_collection', True)
('dropout_2', False)
('final_decision', True)
<bound method Sequential.summary of <keras.engine.sequential.Sequential object at 0x7f2138b2e5d0>>
Found 6000 images belonging to 2 classes.
Found 2000 images belonging to 2 classes.
Epoch 1/5
375/375 [==============================] - 54s 145ms/step - loss: 0.2534 - acc: 0.8900 - val_loss: 0.1626 - val_acc: 0.9335
Epoch 2/5
375/375 [==============================] - 52s 138ms/step - loss: 0.1614 - acc: 0.9350 - val_loss: 0.1398 - val_acc: 0.9415
Epoch 3/5
375/375 [==============================] - 52s 139ms/step - loss: 0.1237 - acc: 0.9523 - val_loss: 0.1635 - val_acc: 0.9395
Epoch 4/5
375/375 [==============================] - 52s 139ms/step - loss: 0.0932 - acc: 0.9605 - val_loss: 0.1481 - val_acc: 0.9365
Epoch 5/5
375/375 [==============================] - 52s 140ms/step - loss: 0.0719 - acc: 0.9730 - val_loss: 0.1572 - val_acc: 0.9365
Model loaded.
('block1_conv1', False)
('block1_conv2', False)
('block1_pool', False)
('block2_conv1', False)
('block2_conv2', False)
('block2_pool', False)
('block3_conv1', False)
('block3_conv2', False)
('block3_conv3', False)
('block3_pool', False)
('block4_conv1', True)
('block4_conv2', True)
('block4_conv3', True)
('block4_pool', True)
('block5_conv1', True)
('block5_conv2', True)
('block5_conv3', True)
('block5_pool', True)
('flatten_3', False)
('final_feature_collection', True)
('dropout_3', False)
('final_decision', True)
<bound method Sequential.summary of <keras.engine.sequential.Sequential object at 0x7f213864bcd0>>
Found 6000 images belonging to 2 classes.
Found 2000 images belonging to 2 classes.
Epoch 1/5
375/375 [==============================] - 58s 156ms/step - loss: 0.0478 - acc: 0.9837 - val_loss: 0.1227 - val_acc: 0.9526
Epoch 2/5
375/375 [==============================] - 57s 152ms/step - loss: 0.0317 - acc: 0.9907 - val_loss: 0.1792 - val_acc: 0.9405
Epoch 3/5
375/375 [==============================] - 57s 152ms/step - loss: 0.0239 - acc: 0.9935 - val_loss: 0.1161 - val_acc: 0.9577
Epoch 4/5
375/375 [==============================] - 57s 152ms/step - loss: 0.0194 - acc: 0.9940 - val_loss: 0.1776 - val_acc: 0.9456
Epoch 5/5
375/375 [==============================] - 57s 152ms/step - loss: 0.0181 - acc: 0.9935 - val_loss: 0.1563 - val_acc: 0.9526











Test loss     : 0.174306627484
Test accuracy : 0.9475